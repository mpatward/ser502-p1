Team Number: 11

Language: Octave

Instructions for Installing Octave:
Open the respective links given below as per the OS you are working on and install Octave by following instructions given on the page.

* [Installing on MAC.](http://wiki.octave.org/Octave_for_MacOS_X)
  
* [Installing on Windows.](http://wiki.octave.org/Octave_for_Microsoft_Windows)

* [Installing on Linux](http://wiki.octave.org/Octave_for_GNU/Linux)

Video:

* [URL](https://www.youtube.com/watch?v=-SQTmZfRfc4&feature=youtu.be)

Repository:
SER502-P1


Thanks
Mandar Patwardhan
Salil Batra
Manasa Mannava